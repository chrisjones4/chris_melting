Short simulations completed: ~ 100,000 time steps

Nb T2300 - Looks like complete melting. Good canditate Temp for a longer sim

Nb T2500 - Complete melting achieved at a lower temp

Pt T1700 - Mostly melted, chose a T of 1800 for a longer sim

Mo T3200:  The particle looks melted at this temperature.  Temp was stepped up from 2900.  Will try shorter sims at 3100 and 3000 before running a 2.5 mil step sim.

Long simulations completed: 2.5 million time steps:

Nb T2300
Pt T1800

Fry Simulations Completed:

Kestrel Simulations Completed:
